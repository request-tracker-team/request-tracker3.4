# RT_SiteConfig.pm
#
# These are the bits you absolutely *must* edit.
#
# To find out how, please read
#   /usr/share/doc/request-tracker3.4/INSTALL.Debian

# THE BASICS:

Set($rtname, 'i.recommend.you.use.your.fqdn');
Set($Organization, 'my.domain.com');

Set($CorrespondAddress , 'rt@my.domain.com');
Set($CommentAddress , 'rt-comment@my.domain.com');

Set($Timezone , 'Europe/London'); # obviously choose what suits you

# THE DATABASE:

Set($DatabaseType, 'Pg'); # e.g. Pg or mysql

# These are the settings we used above when creating the RT database,
# you MUST set these to what you chose in the section above.

Set($DatabaseUser , 'rtuser');
Set($DatabasePassword , 'wibble');
Set($DatabaseName , 'rtdb');

# THE WEBSERVER:

Set($WebPath , "/rt");
Set($WebBaseURL , "http://my.domain.com");

1;
