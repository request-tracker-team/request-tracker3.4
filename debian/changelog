request-tracker3.4 (3.4.5-2) unstable; urgency=low

  * add an alternative dependency on libcgi-fast-perl to rt3.4-apache2.
    (Closes: #409833)
  * Add a note in rt3.4-clients README.Debian about debugging deferred
    deliveries. (Closes: #397783)
  * point to rt3.4-clients instead of rt3-clients for mail configuration. 

 -- Niko Tyni <ntyni@iki.fi>  Mon, 26 Feb 2007 00:10:52 +0200

request-tracker3.4 (3.4.5-1) unstable; urgency=low

  [ Niko Tyni ]
  * New upstream release. (Closes: #355025)
  * Adopting for the new Debian Request Tracker Group. Please join!
    (Closes: #366551)
  * Update minimum dependency on libdbix-searchbuilder-perl to 1.35.
  * Update the recommended postgresql and mysql package versions.
    (Closes: #342732)
  * Fix rt3.4-apache2 package description. (Closes: #330690)
  * Make the build-dependency on dpatch versioned, for
    /usr/share/dpatch/dpatch-run.
  * Add manpages for /usr/bin/rt-3.4 and /usr/sbin/rt-dump-database-3.4.
  * Fix bashism in debian/rules.
  * Update speedycgi and fastcgi configurations to serve images straight
    from the filesystem, bypassing the HTML::Mason handlers.
  * Add apache2-speedycgi.conf.
  * Add apache2-fastcgi.conf. (Closes: #331236, #386722, #327064)

  [ Toni Mueller ]
  * added myself as uploader
  * small fix to debian/rules

 -- Toni Mueller <toni@debian.org>  Sun,  1 Oct 2006 17:07:32 +0200

request-tracker3.4 (3.4.4-3) unstable; urgency=low

  * QA Upload
  * Update apache2-modperl2.conf to new mod-perl2 API
    Closes: #382569
  * Move debhelper and cdbs to Build-Depends
  * Conforms to latest Standards Version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Sat, 12 Aug 2006 13:40:00 +0200

request-tracker3.4 (3.4.4-2) unstable; urgency=low

  * Orphaning this package. Please consult the debian-perl mailing list if
    you are interested in taking up maintainership.

 -- Stephen Quinney <sjq@debian.org>  Tue,  9 May 2006 16:25:04 +0100

request-tracker3.4 (3.4.4-1) unstable; urgency=low

  * New upstream release, closes: #327929.
  * Now supports mod_perl2 again, closes: #317271.
  * Version 3.4.3 introduced an updated German translation,
    closes: #304614, #313826
  * Added new dependency on libtext-wikiformat-perl.
  * Updated minimum dependencies on libhtml-mason-perl to (>= 1.31) and
    libxml-rss-perl to (>= 1.05).
  * Increased minimum recommended postgresql version to 7.4 as some of the
    upgrade scripts do not work with earlier versions.
  * Switched to my debian.org email address.
  * Split out the apache related dependencies config files and
    dependencies into two new packages rt3.4-apache and rt3.4-apache2. The
    main package requires that one of these is installed.
  * Removed notes in README.Debian about request-tracker3.4 only being
    beta quality. It is now much better than the 3.0 or 3.2 series and is
    the only one in sid and etch.
  * Added a comment to README.Debian for rt3.4-clients about turning on
    pipe_transport for exim4, closes: #272630, #327232, #317875.

 -- Stephen Quinney <sjq@debian.org>  Thu, 15 Sep 2005 15:22:32 +0100

request-tracker3.4 (3.4.2-4) unstable; urgency=low

  * Actually fix the rmtree problem in webmux.pl, this really closes
    #309271, hopefully...

 -- Stephen Quinney <sjq@debian.org>  Tue, 17 May 2005 19:44:19 +0100

request-tracker3.4 (3.4.2-3) unstable; urgency=low

  * Added a dependency on libxml-simple-perl, closes: #309458
  * Silenced annoying warning from rmtree when called with an empty list
    of files to remove in webmux.pl, closes: #309271.
  
 -- Stephen Quinney <sjq@debian.org>  Tue, 17 May 2005 13:36:02 +0100

request-tracker3.4 (3.4.2-2) unstable; urgency=high

  * Removed all mention of modperl2 support as the latest upload of
    libapache2-mod-perl2 is incompatible with request-tracker3.4.

 -- Stephen Quinney <sjq@debian.org>  Sun,  8 May 2005 19:44:59 +0100

request-tracker3.4 (3.4.2-1) unstable; urgency=low

  * New upstream release - closes: #305519.
  * Updated minimum dependency on libdbix-searchbuilder-perl to 1.26
  * Updated minimum dependency on libdbd-pg-perl to 1.41
  * Removed rt-standalone_httpd as it has dependencies which we cannot
    fulfill and have not been able to for quite a while. It's in the
    examples directory if you really need it.
  * Updated 04_sitemodules patch to match new webmux style.
  * Removed 20_logourl patch. If you want to show your own logo you should
    use the overlay system as you need to change the href url and
    alternate text as well as the image url.

 -- Stephen Quinney <sjq@debian.org>  Wed,  4 May 2005 09:10:14 +0100

request-tracker3.4 (3.4.1-2) unstable; urgency=high

  * Updated the minimum dependency on libdbix-searchbuilder to 1.21
  * Added a dependency on libcache-simple-timedexpiry-perl, closes: #303698.
  * Urgency high as these bugs will also affect Sarge.
  * Updated the minimum dependency on libhtml-mason-perl to 1.23
  * Fixed upgraded example in README.Debian, closes: #303253.
  * Allow other DBD alternatives in the dependencies, closes: #301937.

 -- Stephen Quinney <sjq@debian.org>  Fri,  8 Apr 2005 10:04:57 +0100

request-tracker3.4 (3.4.1-1) unstable; urgency=high

  * New upstream release
  * Urgency high as this fixes quite a few problems in the previous
    release. See upstream changelog for full details.
  * Fixed prerm which was not removing all the alternatives.
  * Fixed acl.mysql script, we do things a slightly different way in
    Debian.

 -- Stephen Quinney <sjq@debian.org>  Wed, 23 Mar 2005 22:35:01 +0000

request-tracker3.4 (3.4.0-1) unstable; urgency=low

  * Initial Release, closes: #293167.
  * Packaging structure taken from the request-tracker3.2 package version
    3.2.2-2 with the important bits altered where needed.

 -- Stephen Quinney <sjq@debian.org>  Fri, 28 Jan 2005 15:37:24 +0000

